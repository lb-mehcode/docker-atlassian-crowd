FROM openjdk:8-alpine

ARG CROWD_VERSION=3.2.3
ARG VIRTUAL_HOST

# Download (and unpack) Atlassian Crowd
RUN mkdir -p /opt && \
    wget -qO- https://www.atlassian.com/software/crowd/downloads/binary/atlassian-crowd-${CROWD_VERSION}.tar.gz | \
    tar zx -C /opt && \
    mv /opt/atlassian-crowd-${CROWD_VERSION} /opt/atlassian-crowd

# Specify the CROWD_HOME directory
RUN echo "crowd.home=/var/atlassian/crowd" >> /opt/atlassian-crowd/crowd-webapp/WEB-INF/classes/crowd-init.properties

# Configure tomcat to work with nginx for tls termination
RUN apk add --no-cache xmlstarlet && xmlstarlet ed -L \ 
    -i "//Connector" -t attr -n "ProxyName" -v ${VIRTUAL_HOST} \ 
    -i "//Connector" -t attr -n "ProxyPort" -v "443" \ 
    -i "//Connector" -t attr -n "scheme" -v "https" \ 
    /opt/atlassian-crowd/apache-tomcat/conf/server.xml

ENTRYPOINT /opt/atlassian-crowd/start_crowd.sh -fg
EXPOSE 8095
